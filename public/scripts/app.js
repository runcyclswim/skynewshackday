document.addEventListener("DOMContentLoaded", function() {
  var ctx = document.getElementById("timeline").getContext("2d")
    , opts = {
      responsive: true
      , timelineTooltipTemplate: "<%= value %>"
    }
    , myLineChart = null
    ;

    var svc = new DataService();
    svc.fetch(moment().subtract(1, 'months'), moment()).then(function(data) {
      var timeline = new Chart(ctx).Timeline(data, opts);
      timeline.chart.canvas.onclick = function(evt){
        var activePoints = timeline.getPointsAtEvent(evt);
        if (activePoints.length === 5) {
          var info = $('.timeline-info')
            , eventData = events[activePoints[0].label]
            , elementsArr = []
            ;
          if (eventData) {
            elementsArr = [
              $('<h2></h2>').html(eventData.title)
              , $('<img />').attr('src', eventData.img).attr('alt', eventData.title)
              , $('<p></p>').html(eventData.abstract)
              , $('<a>Read more</a>').attr('href', eventData.url)
            ];
          }
          info.html(elementsArr)
        }
      };
    });
});
