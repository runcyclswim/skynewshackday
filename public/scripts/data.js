function DataService() {};
DataService.prototype.fetch = function(from, to) {
  var res = Q.defer();
  from = moment(from).format('YYYY-MM-DD');
  to = moment(to).format('YYYY-MM-DD');
  $.get('/opinionpolls/' + from + '/' + to, function(data) {
    var processedData = {
      labels: data.labels
      , datasets: []
    }
    $.each(data.datasets, function(key, value) {
      var colors = {
        labour: '196, 8, 19'
        , libdem: '247,179,42'
        , other: '153,153,153'
        , torries: '20,101,182'
        , ukip: '160,27,189'
      };
      processedData.datasets.push({
          label: key,
          fillColor: "rgba(" + colors[key] + ",0)",
          strokeColor: "rgba(" + colors[key] + ",1)",
          pointColor: "rgba(" + colors[key] + ",1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(" + colors[key] + ",1)",
          data: value
      });
    });
    res.resolve(processedData);
  });
  return res.promise;
};

var events = {};
events["2014-10-14"] = {
        id: 1361710,
        url: "http://news.sky.com/story/1361710/ukip-hits-new-poll-high-after-1-7bn-eu-bill",
        img: "http://media.skynews.com/media/images/generated/2014/10/28/345367/default/v4/poll-1-300x225.jpg",
        title: "UKIP Hits New Poll High After £1.7bn EU Bill",
        abstract: "As the row over the surcharge from Brussels for the UK rumbles on, UKIP climbs four points to 19% in the latest opinion poll."
};
events["2014-09-28"] = {
        id: 775558,
        url: "http://news.sky.com/story/775558/bigot-gaffe-brown-says-sorry-to-pensioner",
        img: "http://media.skynews.com/media/images/generated/sky-news/content/StaticFile/jpg/2010/Apr/Week4/139612/default/v0/15621646-300x225.jpg",
        title: "'Bigot' Gaffe: Brown Says Sorry To Pensioner",
        abstract: "Gordon Brown has apologised in person to a grandmother after he was caught on microphone in an unguarded moment calling her 'bigoted'.",
        sublinks: [
                {
                        id: 1338658,
                        url: "http://news.sky.com/story/1338658/was-this-the-rehabilitation-of-gordon-brown",
                        title: "Was This The Rehabilitation Of Gordon Brown?"
                }
        ]
};
