var width,
    nbDays;

$(function () {
    nbDays = pollData.length || 1;
    calcDayWidth();

    $( window ).resize(calcDayWidth);
});

function calcDayWidth() {
    width = $(window).width();
    dayWidth = width / nbDays;

    $('.majority .point').css({width: dayWidth});
}