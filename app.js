'use strict';

var express = require('express'),
    app = express(),
    twitter = require('twitter'),
    sentiment = require('sentiment'),
    _ = require('lodash'),
    Promise = require('es6-promise').Promise,
    swig = require('swig'),
    fs = require('fs'),
    moment = require('moment'),

    twit = new twitter({
        consumer_key: 'A4RZLCAF0c0Phq8MWIg55s8eF',
        consumer_secret: 'ilwFpy55P2CiiXP0Y8l9zOIgvBIOjYUACBIYn5c4OMDRTmTp2w',
        access_token_key: '16681805-FjApFXe6yhlhSrFFZOtL6w0etX17heQykwotkn2Nm',
        access_token_secret: 'acA1U9mXft3TcxayuV01gvaCqTmroR9nIY2aSdAiOlkmX'
    }),

    parties = [{
            id: 'conservatives',
            twitter: '@Conservatives',
            name: 'Conservatives'
        },
        {
            id: 'libdems',
            twitter: '@LibDems',
            name: 'Liberal Democrats'
        },
        {
            id: 'labour',
            twitter: '@UKLabour',
            name: 'Labour'
        },
        {
            id: 'ukip',
            twitter: '@ukip',
            name: 'UKIP'
        }],

    twitterSearch = function(terms) {
        return new Promise(function(resolve, reject) {
            twit.search(terms, function(results) {
                return resolve(results);
            });
        });
    };

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', 'views');
app.set('view cache', false);

swig.setDefaults({ cache: false });

app.use(express.static('public'));

app.get('/sentiment', function getSentiment(req, res) {

    var score = 0,
        now = new Date(),
        searches = [],
        i = 0;

	_.forEach(parties, function(party) {
		searches.push(twitterSearch(party.twitter + ' OR #' + party.name));
	});

	Promise.all(searches).then(function(results) {

		var items = [],
			conservatives = 0,
			libdems = 0,
			labour = 0,
			ukip = 0;

		_.forEach(results, function(result) {
			var id = parties[i].id;

			_.forEach(result.statuses, function(item) {
				var sent = sentiment(item.text);

				items.push({
					id: id,
					score: sent.score
				});

			});

			i++;
		});

		_.forEach(items, function(item) {
			if (item.id === 'conservatives') {
				conservatives += item.score;
			} else if (item.id === 'libdems') {
				libdems += item.score;
			} else if (item.id === 'labour') {
				labour += item.score;
			} else if (item.id === 'ukip') {
				ukip += item.score;
			}
		});

		res.json([
			{
				date: moment(now).format('YYYY-MM-DD'),
				parties: [
					{
						id: 'conservatives',
						label: 'Conservatives',
						percentage: (60 / 100) * conservatives,
						score: conservatives
					},
					{
						id: 'libdems',
						label: 'Liberal Democrats',
						percentage: (60 / 100) * libdems,
						score: libdems
					},
					{
						id: 'labour',
						label: 'Labour',
						percentage: (60 / 100) * labour,
						score: labour
					},
					{
						id: 'ukip',
						label: 'UKIP',
						percentage: (60 / 100) * ukip,
						score: ukip
					}
				]
			}
		]);

	});

});

app.get('/mentions', function getMention(req, res) {

    var mentions = [];

	_.forEach(parties, function(party) {
		mentions.push(twitterSearch(party.twitter + ' OR #' + party.name));
	});

	Promise.all(mentions).then(function(results) {
		var tweets = [],
			munge,
			conservatives,
			libdems,
			labour,
			ukip,
			totalCount,
			now = new Date();

		_.forEach(results, function(result) {
			_.forEach(result.statuses, function(tweet) {
				tweets.push(tweet.text);
			});
		});

		munge = tweets.join('').toLowerCase();

		conservatives = munge.split('conservatives').length;
		libdems = munge.split('libdems').length;
		labour = munge.split('labour').length;
		ukip = munge.split('ukip').length;

		totalCount = conservatives + libdems + labour + ukip;

		res.json([{
			date: moment(now).format('YYYY-MM-DD'),
			parties: [
				{
					id: 'conservatives',
					label: 'Conservatives',
					percentage: (totalCount / 100) * conservatives
				},
				{
					id: 'libdems',
					label: 'Liberal Democrats',
					percentage: (totalCount / 100) * libdems
				},
				{
					id: 'labour',
					label: 'Labour',
					percentage: (totalCount / 100) * labour
				},
				{
					id: 'ukip',
					label: 'UKIP',
					percentage: (totalCount / 100) * ukip
				}
			]
		}]);
	});

});

app.get('/opinionpolls/:start/:end', function getPolls(req, res) {
    fs.readFile(__dirname + '/public/data/polls.json', function (err, data) {
        if (err) throw err;

        var jsonData = JSON.parse(data),
            chartData = {},
            labels = [],
            datasets = {
                torries: [],
                labour: [],
                libdem: [],
                ukip: [],
                other: []
            };

        var startDate = req.params.start ? moment(req.params.start) : moment('2012-04-16'),
            endDate = req.params.end ? moment(req.params.end) : moment();
        _.each(jsonData, function(day) {
            var currentDate = moment(day.date);
            if(currentDate.isAfter(startDate) && currentDate.isBefore(endDate)) {
                labels.unshift(day.date);

                _.each(['torries', 'labour', 'libdem', 'ukip', 'other'], function(party) {
                    datasets[party].unshift(day.percent[party]);
                });
            }
        });

        res.status(200).json({labels: labels, datasets: datasets});
    });
});

app.get('/', function (req, res) {
    res.render('main');
});

app.get('/embed', function (req, res) {
    fs.readFile(__dirname + '/public/data/polls.json', function (err, data) {
        if (err) throw err;

        var jsonData = JSON.parse(data).slice(0, 14);

        res.render('embed', {
            data: _(jsonData).reverse().value()
        });
    });
});

var server = app.listen(3000, function() {
    console.log('Hack day app listening at http://localhost:%s', server.address().port);
});
