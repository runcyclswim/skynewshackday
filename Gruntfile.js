'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    nodeunit: {
      files: ['test/**/*_test.js'],
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib: {
        src: ['lib/**/*.js']
      },
      test: {
        src: ['test/**/*.js']
      },
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib: {
        files: '<%= jshint.lib.src %>',
        tasks: ['jshint:lib', 'nodeunit']
      },
      public: {
        files: [
          'public/scripts/*.js',
          ],
        options: {
          livereload: true
        }
      },
      views: {
        files: [
          'views/*.html',
          ],
        options: {
          livereload: true
        }
      },
      test: {
        files: '<%= jshint.test.src %>',
        tasks: ['jshint:test', 'nodeunit']
      },
      sass: {
          files: 'public/**/*.scss',
          tasks: 'sass:dev',
          options: {
              spawn: false
          }
      }
    },
    sass: {
      dev: {
            options: {
                // quiet: true,
                style: 'compressed',
                sourcemap: true,
                precision: 3
            },
            files: {
                'public/styles/main.css':'public/styles/sass/main.scss'
            }
        }
    },
    nodemon: {
      dev: {
        script: 'app.js'
      }
    },
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');

  // Default task.
  grunt.registerTask('default', ['jshint', 'sass', 'nodemon', 'watch']);

};
